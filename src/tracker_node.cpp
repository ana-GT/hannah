#include <hannah/tracker_node.h>

double fik_min( const std::vector<double> &_x, 
		std::vector<double> &_grad,
		void* _data )
{
  Tracker* tracker = (Tracker*)_data;
  return tracker->function_to_minimize(_x, _grad);
}

void fineq( uint _m,
	    double* _result,
	    uint _n,
	    const double *_x,
	    double *_grad,
	    void *_data ) {

  Tracker* tracker = (Tracker*)_data;
  for( uint i = 0; i < _m; ++i )
  {
    double dqi = fabs( _x[i] - tracker->last_q_(i) );
    _result[i] = dqi - tracker->vel_lim_[i]*tracker->get_dt();
  }

  if( _grad != NULL ) {
    std::vector<double> v1(_m);
    std::vector<double> vals(_n);
    for( uint i = 0; i < _n; ++i ) { vals[i] = _x[i]; }
    double jump = boost::math::tools::epsilon<float>();
    
    for( uint i = 0; i < _n; ++i ) {
      double o = vals[i];
      vals[i] = o + jump;
      for( uint j = 0; j < _m; ++j ) {
	double dqj = fabs( vals[j] - tracker->last_q_(j) );
	v1[j] = dqj - tracker->vel_lim_[j]*tracker->get_dt();
	vals[i] = o;
	_grad[j*_n + i] = (v1[j] - _result[j] )/jump;
      }
      
    }
  }
}

double Tracker::function_to_minimize( const std::vector<double> &_x,
				      std::vector<double> &_grad )
{
  std::vector<double> grad_p(_x.size(), 0.0);
  std::vector<double> grad_o(_x.size(), 0.0);
  std::vector<double> grad_v(_x.size(), 0.0);
  std::vector<double> grad_e(_x.size(), 0.0);
  
  double result =  wp_*fp(_x, grad_p) + wo_*fo(_x, grad_o) + wv_*fv(_x, grad_v) + we_*fe(_x, grad_e);

  for( int i = 0; i < _grad.size(); ++i ) {
    _grad[i] = wp_*grad_p[i] + wo_*grad_o[i] + wv_*grad_v[i] + we_*grad_e[i];
  }
  return result;
}

/////////////////////////////////////////////////////////
double Tracker::fp( const std::vector<double> &_x,
		    std::vector<double> &_grad )
{
  Eigen::Vector3d error_p;
  error_p << target_pos_ - fk_position(_x);
  double result = Freg( 1, 0.0, 5.0, 0.2, error_p.norm() );

  double jump = boost::math::tools::epsilon<float>();

  if( !_grad.empty() ) {
    
    Eigen::Vector3d error_pi;
    std::vector<double> vals(_x);
    
    for( int i = 0; i < _x.size(); ++i ) {
      double original = vals[i];
      vals[i] = original + jump;

      error_pi = target_pos_ - fk_position(vals);
      _grad[i] = ( Freg(1, 0.0, 5.0, 0.2, error_pi.norm()) - result ) / jump;
      vals[i] = original;
    }
  }
  
  return result;
}

/**
 * @function fo
 * @brief Rotation
 */
double Tracker::fo( const std::vector<double> &_x,
		    std::vector<double> &_grad )
{
  Eigen::Quaterniond fkq = fk_rotation(_x);
  Eigen::AngleAxisd diff(target_rot_.inverse()*fkq);
  Eigen::AngleAxisd diffa(target_rot_.inverse()*(Eigen::Quaterniond(-fkq.w(), -fkq.x(), -fkq.y(), -fkq.z())) );

  double error_q = std::min( fabs(diff.angle()), fabs(diffa.angle()) );  
  double result = Freg( 1, 0.0, 5.0, 0.2, error_q );

  double jump = boost::math::tools::epsilon<float>();

  if( !_grad.empty() ) {
    
    std::vector<double> vals(_x);
    double error_qi;
    for( int i = 0; i < _x.size(); ++i ) {

      double original = vals[i];
      vals[i] = original + jump;

      fkq = fk_rotation(vals);
      diff = Eigen::AngleAxisd(target_rot_.inverse()*fkq);
      diffa = Eigen::AngleAxisd(target_rot_.inverse()*(Eigen::Quaterniond(-fkq.w(), -fkq.x(), -fkq.y(), -fkq.z())) );

      error_qi = std::min( fabs(diff.angle()), fabs(diffa.angle()) );  

      _grad[i] = ( Freg(1, 0.0, 5.0, 0.2, error_qi) - result ) / jump;
      vals[i] = original;
    }
  }
  
  return result;
}


// Minimize joint velocity
double Tracker::fv( const std::vector<double> &_x,
		    std::vector<double> &_grad )
{
  if( _x.size() != last_q_.size() ) { return 0; }
  
  Eigen::VectorXd dq(_x.size());
  for(int i=0; i < _x.size(); ++i){ dq(i) = _x[i] - last_q_(i); }

  double result = Freg( 1, 0.0, 5.0, 0.2, dq.norm());

  double jump = boost::math::tools::epsilon<float>();
  if( !_grad.empty() ) {
    
    std::vector<double> vals(_x);
    for( int i = 0; i < _x.size(); ++i ) {

      double original = vals[i];
      vals[i] = original + jump;

      for(int i=0; i < vals.size(); ++i){ dq(i) = vals[i] - last_q_(i); }
      _grad[i] = ( Freg(1, 0.0, 5.0, 0.2, dq.norm()) - result ) / jump;
      vals[i] = original;
    }
  } // if( !_grad.empty() )
  
  return result;
}

// Minimize ee velocity
double Tracker::fe( const std::vector<double> &_x,
		    std::vector<double> &_grad )
{
  Eigen::Vector3d dx;
  dx = last_x_ - fk_position(_x);

  double result = Freg( 1, 0.0, 5.0, 0.2, dx.norm() );

  double jump = boost::math::tools::epsilon<float>();
  if( !_grad.empty() ) {
    
    std::vector<double> vals(_x);
    for( int i = 0; i < _x.size(); ++i ) {

      double original = vals[i];
      vals[i] = original + jump;

      dx = last_x_ - fk_position(vals);
      _grad[i] = ( Freg(1, 0.0, 5.0, 0.2, dx.norm()) - result ) / jump;
      vals[i] = original;
    }
  } // if( !_grad.empty() )

  return result;
}

// Smooth
double Tracker::Freg( int _n, double _s,
		      double _r, double _c,
		      double _fx ) {

  double signo = 1;
  if( _n %2 == 1 ) { signo = -1; }
  
  return signo*exp( -pow( fabs(_fx-_s), 2)/(2*_c*_c) ) + _r*pow( fabs(_fx - _s), 4 ); 
}


Eigen::Vector3d Tracker::fk_position( const std::vector<double> &_x )
{
  KDL::Frame Tf_ee;
  KDL::JntArray q(_x.size() );
  for( int i = 0; i < _x.size(); ++i ) { q(i) = _x[i]; }  
  subject_.get_limb().fk_solver->JntToCart(q, Tf_ee);
  
  return Eigen::Vector3d( Tf_ee.p.x(), Tf_ee.p.y(), Tf_ee.p.z() );
}

Eigen::Quaterniond Tracker::fk_rotation( const std::vector<double> &_x )
{
  KDL::Frame Tf_ee;
  KDL::JntArray q(_x.size() );
  for( int i = 0; i < _x.size(); ++i ) { q(i) = _x[i]; }  
  subject_.get_limb().fk_solver->JntToCart(q, Tf_ee);

  double qx, qy, qz, qw;
  Tf_ee.M.GetQuaternion( qx, qy, qz, qw );
  return Eigen::Quaterniond( qw, qx, qy, qz );
}


Tracker::Tracker( ros::NodeHandle &_nh ) :
  nh_(_nh),
  subject_(nh_)
  
{
  is_tracking_ = false;
  js_setup_ = false;
  last_cmd_time_ = ros::Time::now();
  server_start_tracking_ = nh_.advertiseService("/start_tracking",
						&Tracker::start_tracking, this);
  server_stop_tracking_ = nh_.advertiseService("/stop_tracking",
					       &Tracker::stop_tracking, this);

}

bool Tracker::init( std::string _js_cmd,
		    std::string _jt_cmd )
{
  if( !subject_.load_subject_params() )
    return false;

  base_link_ = subject_.get_limb().base_link;
  grasp_link_ = subject_.get_limb().grasp_link;
  
  bool setup = false;
  if( !_js_cmd.empty() )
  {
    cmd_js_pub_ = nh_.advertise<sensor_msgs::JointState>( _js_cmd, 100 );
    js_setup_ = true;
    setup = true;
  }
  if( !_jt_cmd.empty() )
  {
    cmd_jt_pub_ = nh_.advertise<trajectory_msgs::JointTrajectory>( _jt_cmd, 1 );
    js_setup_ = false;
    setup =true;
  }

  // Optimizer
  init_optimizer();
  
  return setup;
}

/**
 * @function init_optimizer
 */
bool Tracker::init_optimizer()
{
  int n = subject_.get_limb().num_dofs;
  // Set time
  dt_ = 0.01; // 100 Hz

  // Optimizer
  optimizer_ .reset( new nlopt::opt( nlopt::LD_SLSQP, n ) );
  
  // Set function to min/max
  optimizer_->set_min_objective( fik_min, this );

  // Get min/max limits
  get_limits( lower_lim_, upper_lim_, vel_lim_ );
  optimizer_->set_lower_bounds( lower_lim_ );
  optimizer_->set_upper_bounds( upper_lim_ );
  
  optimizer_->set_maxtime(0.005);
  // Set tolerance in minimizing function
  std::vector<double> tolerance(1, 1e-8); //boost::math::tools::epsilon<float>());
  optimizer_->set_xtol_abs(tolerance[0]);
  // Set inequality constraint;
  std::vector<double> tol_array( n, boost::math::tools::epsilon<float>() );
  optimizer_->add_inequality_mconstraint( fineq, this, tol_array );

  // Set weights
  wp_ = 10.0; wo_ = 9.0; wv_ = 5.0; we_ = 2.0;
  //wp_ = 50.0; wo_ = 40.0; wv_ = 0.1; we_ = 0.1;
}


bool Tracker::get_limits( std::vector<double> &_lower_lim,
			  std::vector<double> &_upper_lim,
			  std::vector<double> &_vel_lim) {
  
  std::map<std::string,double> lower_limits;
  std::map<std::string,double> upper_limits;
  
  std::vector<std::string> joints;
  joints = subject_.get_limb().joint_group->joints;
  lower_limits = subject_.get_limb().joint_group->joint_lower_limits;
  upper_limits = subject_.get_limb().joint_group->joint_upper_limits;

  _lower_lim.clear();
  _upper_lim.clear();
  _vel_lim.clear();
  
  for( int i = 0; i < joints.size(); ++i ) {
    _lower_lim.push_back( lower_limits[joints[i]] );
    _upper_lim.push_back( upper_limits[joints[i]] );
  }
  
  _vel_lim = subject_.get_limb().joint_group->velocities;
  return true;
}
  
bool Tracker::stop_tracking(hannah::StopTracking::Request  &req,
			    hannah::StopTracking::Response &res)
{
  is_tracking_ = false;
  return true;
}


bool Tracker::start_tracking(hannah::StartTracking::Request  &req,
			     hannah::StartTracking::Response &res)
{
  track_frame_ = req.tracking_frame;
  is_tracking_ = true;
  res.success = true;
  return true;
}

bool Tracker::spin()
{
  if( is_tracking_ )
  {
    // Get frame
    KDL::Frame goal, current;
    if(!getPose( base_link_, track_frame_, goal )) { return false; }
    if(!getPose( base_link_, grasp_link_, current )) { return false; }

    // Send command
    KDL::JntArray qs;
    sensor_msgs::JointState js;
    
    Eigen::MatrixXd  Jlin, Jang, Jlin_inv, Jang_inv;
    Eigen::MatrixXd Jt, mult;
    Eigen::VectorXd dtheta;
    Eigen::VectorXd dx;
    
    subject_.get_joint_array(qs);
    subject_.get_joint_state(js);

    std::vector<double> xs(qs.data.size()); double opt_f;
    for( int i = 0; i < qs.data.size(); ++i ) { xs[i] = qs(i); }

    
    // Set target
    target_pos_ << goal.p.x(), goal.p.y(), goal.p.z();
    double rx, ry, rz, rw; goal.M.GetQuaternion( rx, ry, rz, rw );
    target_rot_ = Eigen::Quaterniond( rw, rx, ry, rz );
    // Set current
    last_x_ << current.p.x(),  current.p.y(), current.p.z();
    last_q_ = qs.data;
    
    // Optimize
    try {
      int res = optimizer_->optimize( xs, opt_f );
    } catch( nlopt::roundoff_limited ) {
      printf("Roundoff limited \n");
    } catch( std::invalid_argument ) {
      printf("Invalid argument \n");
    }

    if( js.position.size() == xs.size() )
    { printf("A1: ");
      for( int i = 0; i < xs.size(); ++i ) {
        printf(" %f ", xs[i] - js.position[i] );
	js.position[i] = xs[i];
      } printf("\n");
    }     
    
    dx = target_pos_ - Eigen::Vector3d(current.p.x(), current.p.y(), current.p.z());
    
    if( dx.norm() > 0.01 )
      { dx.normalize();
	dx *= 0.01;// normalize step 5mm
      }
    subject_.get_limb().getJacs(qs, Jlin, Jang, Jlin_inv, Jang_inv);
    Jt = Jlin.transpose();
    Eigen::MatrixXd Jm;
    pInv( Jlin*Jt, Jm );
    mult = Jt*Jm;
    dtheta = mult*dx;
    if( dtheta.size() != js.position.size() ) { return false; }
    printf("A2: ");
    for( int i = 0; i < dtheta.size(); ++i ) {
      js.position[i] += dtheta(i);
      printf("%f ", dtheta(i) );
      } printf("\n");

    if( js_setup_ ) {
      cmd_js_pub_.publish(js);
    } else {
      trajectory_msgs::JointTrajectory traj = stream_traj(js);
      cmd_jt_pub_.publish(traj);
    }

    // Last cmd time
    last_cmd_time_ = ros::Time::now();

  }
  else
  {
    // Do nothing
  }
  return true;
}

trajectory_msgs::JointTrajectory Tracker::stream_traj(const sensor_msgs::JointState& msg) {

  trajectory_msgs::JointTrajectory traj;
  trajectory_msgs::JointTrajectoryPoint p;

  //traj.header.stamp = ros::Time::now();
  for(int i = 0; i < msg.name.size(); ++i){
    traj.joint_names.push_back(msg.name[i]);
  }
  for(int i = 0; i < msg.position.size(); ++i){
    p.positions.push_back(msg.position[i]);
  } 
  //p.time_from_start=ros::Duration(dt_);
  //double d = (ros::Time::now() - last_cmd_time_).toSec(); printf(" -- dt: %f \n", d);
  p.time_from_start= ros::Duration(dt_);
  traj.points.push_back(p);

  return traj;
}


/**
 * @function getPose
 */
bool Tracker::getPose( std::string _source_frame,
		       std::string _target_frame,
		       KDL::Frame &_goal ) {

  tf::StampedTransform Tfx;
  try {
    tf_.waitForTransform( _source_frame, _target_frame, ros::Time(0), ros::Duration(0.1) );
    tf_.lookupTransform( _source_frame, _target_frame, ros::Time(0), Tfx );
  } catch (tf::TransformException ex) {
    ROS_ERROR("%s",ex.what());
    return false;
  }
  
  tf::poseTFToKDL( Tfx, _goal );  
  return true;
}


///////////////////////////////////////
int main(int argc, char **argv)
{
  ros::init(argc, argv, "tracker_node");
  ros::NodeHandle nh("~");

  Tracker tr(nh);
  
  std::string js_cmd, jt_cmd;
  if( !nh.hasParam("js_cmd") || !nh.hasParam("jt_cmd") )
  {
    ROS_ERROR("Need to set params js_cmd and jt_cmd (one of them must be empty");
    return 1;
  }
  
  nh.getParam("js_cmd", js_cmd);
  nh.getParam("jt_cmd", jt_cmd);
  
  tr.init( js_cmd, jt_cmd );

  int n = (int)(1.0/(double)tr.get_dt() );
  ros::Rate r(n);
  while( ros::ok() )
  {
    ros::spinOnce();
    r.sleep();
    tr.spin();
  }
}
