
#include <hannah/tracker_im.h>
#include <hannah/StartTracking.h>
#include <hannah/StopTracking.h>

TrackerIM::TrackerIM( ros::NodeHandle &_nh ) :
  nh_(_nh),
  server_("tracker_server") {

  // Add controls to menu
  std::vector<std::string> menu_items;
  menu_items.push_back("Start tracking");
  menu_items.push_back("Stop tracking");
  menu_items.push_back("Sync to EE");
  for( auto mi : menu_items )
    menu_id_[mi] = menu_handler_.insert(mi, boost::bind( &TrackerIM::menu_feedback, this, _1 ) );

  
  client_start_tracking_ = nh_.serviceClient<hannah::StartTracking>("/start_tracking");
  client_stop_tracking_ = nh_.serviceClient<hannah::StopTracking>("/stop_tracking");
  
}

void TrackerIM::menu_feedback(  const visualization_msgs::InteractiveMarkerFeedbackConstPtr &_feedback ) {

  if( _feedback->event_type == visualization_msgs::InteractiveMarkerFeedback::MENU_SELECT ) {
    if( _feedback->menu_entry_id == menu_id_["Start tracking"] ) {
      hannah::StartTracking srv;
      srv.request.tracking_frame = marker_frame_;
      client_start_tracking_.call(srv);
    }
    if( _feedback->menu_entry_id == menu_id_["Stop tracking"] ) {
      hannah::StopTracking srv;
      client_stop_tracking_.call(srv);
    }
    if( _feedback->menu_entry_id == menu_id_["Sync to EE"] ) {
      geometry_msgs::Pose pose;
      std_msgs::Header header;
      //header.frame_id = reference_frame_; // Frame is always w.r.t. referenceframe_
      //header.stamp = ros::Time::now();
      if( getPose( reference_frame_,
		   sync_frame_,
		   pose ) )
      {
	server_.setPose( marker_frame_,
			 pose,
			 header );
	server_.applyChanges();
      } // if getPose
      
    } // if _feedback->menu_entry Sync To EE
    

  }

  
}


bool TrackerIM::init( std::string _sync_frame,
		      std::string _reference_frame,
		      std::string _marker_frame ) {
  sync_frame_ = _sync_frame;
  reference_frame_ = _reference_frame;
  marker_frame_ = _marker_frame;

  ROS_WARN("Sync: %s ref: %s marker: %s ",
	   sync_frame_.c_str(),
	   reference_frame_.c_str(),
	   marker_frame_.c_str() );
  // Add pick marker
  visualization_msgs::InteractiveMarker im_track;
  printf("Create 3d marker \n");
  im_track = create_3D_marker( marker_frame_, 0.0, 0.0, 1.0, 1.0  ); // Blue color!
  printf("Ad interactive marker \n");
  add_interactive_marker( im_track ); 

  menu_handler_.apply( server_, im_track.name );
  server_.applyChanges();
  printf("Added marker \n");
}

/**
 * @function getPose
 */
bool TrackerIM::getPose( std::string _source_frame,
			 std::string _target_frame,
			 geometry_msgs::Pose &_pose ) {

  // Init
  _pose.orientation.x = 0.0;
  _pose.orientation.y = 0.0;
  _pose.orientation.z = 0.0;
  _pose.orientation.w = 1.0;

  tf::StampedTransform Tfx;
  try {
    ros::Time ti = ros::Time(0);
    tf_.waitForTransform( _source_frame, _target_frame, ti, ros::Duration(1.0) );
    tf_.lookupTransform( _source_frame, _target_frame, ti, Tfx );
  } catch (tf::TransformException ex) {
    ROS_ERROR("%s",ex.what());
    return false;
  }

  geometry_msgs::TransformStamped stamped_transform;
  tf::transformStampedTFToMsg( Tfx, stamped_transform );
  _pose.position.x = stamped_transform.transform.translation.x;
  _pose.position.y = stamped_transform.transform.translation.y;
  _pose.position.z = stamped_transform.transform.translation.z;
  _pose.orientation.x = stamped_transform.transform.rotation.x;
  _pose.orientation.y = stamped_transform.transform.rotation.y;
  _pose.orientation.z = stamped_transform.transform.rotation.z;
  _pose.orientation.w = stamped_transform.transform.rotation.w;
  
  return true;
}


/**
 * @function create_3D_marker
 */
visualization_msgs::InteractiveMarker TrackerIM::create_3D_marker( std::string _name,
								   double _r,
								   double _g,
								   double _b,
								   double _a ) {
  visualization_msgs::InteractiveMarker im;
  im.header.frame_id = reference_frame_;
  //im.header.stamp=ros::Time::now();
  im.name = _name;
  im.description = "Tracker IM";

  if( !getPose( reference_frame_, sync_frame_, im.pose ) ) {
    ROS_WARN("TrackerIM::create_3D_marker: Trouble getting pose between %s and %s", reference_frame_.c_str(), sync_frame_.c_str() );
  }

  im.scale = 0.5; 
  
  // create a grey box marker
  visualization_msgs::Marker marker;
  marker.type = visualization_msgs::Marker::SPHERE;

  marker.scale.x = 0.05;
  marker.scale.y = 0.05;
  marker.scale.z = 0.05;
  
  marker.color.r = _r; marker.color.g = _g;
  marker.color.b = _b; marker.color.a = _a;

  // create a non-interactive control which contains the box
  visualization_msgs::InteractiveMarkerControl viz_control;
  viz_control.always_visible = true;
  viz_control.markers.push_back( marker );
  viz_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_ROTATE_3D;
  // add the control to the interactive marker
  im.controls.push_back( viz_control );

  // create a control which will move the box
  // this control does not contain any markers,
  // which will cause RViz to insert two arrows
  add_6D_controls( im );

  return im;
}


/**
 * @function add_6D_controls
 */
void TrackerIM::add_6D_controls( visualization_msgs::InteractiveMarker &_im ) {
  
  visualization_msgs::InteractiveMarkerControl move_control;

  tf::Quaternion orien(1.0, 0.0, 0.0, 1.0);
  orien.normalize();
  tf::quaternionTFToMsg(orien, move_control.orientation);
  move_control.name = "rotate_x";
  move_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::ROTATE_AXIS;
  _im.controls.push_back(move_control);

  move_control.name = "move_x";
  move_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_AXIS;
  _im.controls.push_back(move_control);

  orien = tf::Quaternion(0.0, 1.0, 0.0, 1.0 );
  orien.normalize();
  tf::quaternionTFToMsg(orien, move_control.orientation);
  move_control.name = "rotate_z";
  move_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::ROTATE_AXIS;
  _im.controls.push_back(move_control);
  move_control.name = "move_z";
  move_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_AXIS;
  _im.controls.push_back(move_control);

  orien = tf::Quaternion(0.0, 0.0, 1.0, 1.0 );
  orien.normalize();
  tf::quaternionTFToMsg(orien, move_control.orientation);
  move_control.name = "rotate_y";
  move_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::ROTATE_AXIS;
  _im.controls.push_back(move_control);

  move_control.name = "move_y";
  move_control.interaction_mode = visualization_msgs::InteractiveMarkerControl::MOVE_AXIS;
  _im.controls.push_back(move_control);


}

/**
 * @function add_interactive_marker
 */
void TrackerIM::add_interactive_marker( visualization_msgs::InteractiveMarker _im ) {

  server_.insert( _im, boost::bind(&TrackerIM::process_feedback, this,_1) );
  server_.applyChanges();
}

/**
 * @function process_feedback
 */
void TrackerIM::process_feedback( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &_feedback ) {

  server_.setPose(_feedback->marker_name,
		  _feedback->pose);
  server_.applyChanges();  
}


void TrackerIM::spin() {
  visualization_msgs::InteractiveMarker im;
  server_.get(marker_frame_, im);
  geometry_msgs::Pose pose = im.pose;
  
  tf::Transform transform;
  transform.setOrigin(tf::Vector3(pose.position.x, pose.position.y, pose.position.z));
  transform.setRotation(tf::Quaternion(pose.orientation.x, pose.orientation.y, pose.orientation.z, pose.orientation.w));
  
  tf_broadcaster_.sendTransform( tf::StampedTransform(transform,
						      ros::Time::now(),
						      im.header.frame_id,
						      marker_frame_) );
}
