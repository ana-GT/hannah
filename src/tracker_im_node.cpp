#include <hannah/tracker_im.h>


int main( int argc, char* argv[] ) {

  ros::init(argc, argv, "tracker_im_node");
  ros::NodeHandle nh("~");
  
  TrackerIM ti(nh);

  std::string sync_frame, reference_frame, marker_frame;
  if( !nh.hasParam("sync_frame") ) {
    ROS_ERROR("sync_frame parameter is not set. Exiting");
    return 1;
  }
  if( !nh.hasParam("reference_frame") ) {
    ROS_ERROR("reference_frame parameter is not set. Exiting");
    return 1;
  }
  if( !nh.hasParam("marker_frame") ) {
    ROS_ERROR("marker_frame parameter is not set. Exiting");
    return 1;
  }
  nh.getParam( "sync_frame", sync_frame );
  nh.getParam( "reference_frame", reference_frame );
  nh.getParam( "marker_frame", marker_frame );
  
  // Init
  ti.init( sync_frame, reference_frame, marker_frame);

  // Spin
  ros::Rate r(100);
  while( ros::ok ) {
    ros::spinOnce();
    r.sleep();
    ti.spin();
  }
  return 0;
}
