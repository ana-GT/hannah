#pragma once

#include <ros/ros.h>
#include <trajectory_msgs/JointTrajectory.h>
#include <sensor_msgs/JointState.h>
#include <tf/transform_listener.h>
#include <hannah/StartTracking.h>
#include <hannah/StopTracking.h>

#include <reachability_representation/subject_t.h>

#include <nlopt.hpp>

/**
 * @class Tracker
 */
class Tracker {

 public:
  Tracker( ros::NodeHandle &_nh );

  bool stop_tracking(hannah::StopTracking::Request  &req,
		     hannah::StopTracking::Response &res);
  bool start_tracking(hannah::StartTracking::Request  &req,
		      hannah::StartTracking::Response &res);
  bool spin();
  bool init( std::string _js_cmd,
	     std::string _jt_cmd );
  bool getPose( std::string _source_frame,
		std::string _target_frame,
		KDL::Frame &_goal );

  bool init_optimizer();
  double function_to_minimize( const std::vector<double> &_x,
			       std::vector<double> &_grad );
  // Opt
  // Minimize ee error
  double fp( const std::vector<double> &_x, std::vector<double> &_grad );
  double fo( const std::vector<double> &_x, std::vector<double> &_grad );
  // Minimize joint velocity
  double fv( const std::vector<double> &_x, std::vector<double> &_grad  );
  // Minimize ee velocity
  double fe( const std::vector<double> &_x, std::vector<double> &_grad  );
  
  // Helpers
  Eigen::Vector3d fk_position( const std::vector<double> &_x );
  Eigen::Quaterniond fk_rotation( const std::vector<double> &_x );
  bool get_limits( std::vector<double> &_lower_lim,
		   std::vector<double> &_upper_lim,
		   std::vector<double> &_vel_lim );
  
  double Freg( int _n, double _s,
	       double _r, double _c,
	       double _fx );

  double get_dt() { return dt_; }
  trajectory_msgs::JointTrajectory stream_traj(const sensor_msgs::JointState& msg);
  // Lim
  std::vector<double> lower_lim_;
  std::vector<double> upper_lim_;
  std::vector<double> vel_lim_;

  // Target to minimize
  Eigen::Vector3d target_pos_;
  Eigen::Quaterniond target_rot_;
  // Current
  Eigen::VectorXd last_q_;
  Eigen::Vector3d last_x_;
  // Other
  double dt_;

  
 protected:
  ros::NodeHandle nh_;
  Subject_t subject_;
  tf::TransformListener tf_;

  std::string track_frame_;
  std::string base_link_;
  std::string grasp_link_;
  
  bool is_tracking_;
  bool js_setup_;
  
  ros::ServiceServer server_start_tracking_;
  ros::ServiceServer server_stop_tracking_;

  ros::Publisher cmd_js_pub_;
  ros::Publisher cmd_jt_pub_;
  ros::Time last_cmd_time_;
  
  // Optimizer
  std::shared_ptr<nlopt::opt> optimizer_;
  // Weights
  double wp_;
  double wv_;
  double we_;
  double wo_;
  
};
