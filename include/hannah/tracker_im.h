#pragma once

#include <ros/ros.h>
#include <interactive_markers/interactive_marker_server.h>
#include <interactive_markers/menu_handler.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>

class TrackerIM {

 public:
  TrackerIM( ros::NodeHandle &_nh );
  bool init( std::string sync_frame,
	     std::string reference_frame,
	     std::string output_frame );
  void spin();

  bool getPose( std::string _source_frame,
		std::string _target_frame,
		geometry_msgs::Pose &_pose );
  
  void menu_feedback(  const visualization_msgs::InteractiveMarkerFeedbackConstPtr &_feedback );

  visualization_msgs::InteractiveMarker create_3D_marker( std::string _name,
							  double _r,
							  double _g,
							  double _b,
							  double _a );
  void add_6D_controls( visualization_msgs::InteractiveMarker &_im );
  void add_interactive_marker( visualization_msgs::InteractiveMarker _im );
  void process_feedback( const visualization_msgs::InteractiveMarkerFeedbackConstPtr &_feedback );

 protected:
  std::string sync_frame_;
  std::string reference_frame_;
  std::string marker_frame_;

  ros::ServiceClient client_start_tracking_;
  ros::ServiceClient client_stop_tracking_;
  ros::NodeHandle nh_;

  tf::TransformListener tf_;
  tf::TransformBroadcaster tf_broadcaster_;

  interactive_markers::InteractiveMarkerServer server_;
  interactive_markers::MenuHandler menu_handler_;
  std::map<std::string,int> menu_id_;
  
};
